const findTweet = elem => {
	let e = elem
	while(e.tagName !== "ARTICLE")
		e = e.parentElement
	return e
}

const remove = () => {
	[...document.querySelectorAll("article svg + *")]
		.filter(e => e.innerText.includes("Promoted"))
		.map(e => findTweet(e))
		.filter(e => e != null)
		.forEach(e => {
			const p = e.parentElement
			p.removeChild(e)
			p.setAttribute("style", "height: 0")
		})
}


let doingChanges = false

const tryRemove = () => {
	if(doingChanges) return
	doingChanges = true
	
	console.log("[no-promoted-tweets] Removing promoted tweets...")
	remove()

	setTimeout(() => doingChanges = false, 200)
}

setInterval(tryRemove, 1000)
